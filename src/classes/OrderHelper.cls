public class OrderHelper {

    /**
     * @name AfterUpdate
     * @description 
     * @param List<Order> newList
     * @param List<Order> oldList
     * @return void
    **/
    public static void AfterUpdate(List<Order> newList, List<Order> oldList) {
        Set<Id> orderIds = new Set<Id>();
        for ( Integer i=0; i<newList.size(); i++ ){
            if ( newList[i].Status == Constants.ACTIVATED_ORDER_STATUS && oldList[i].Status == Constants.DRAFT_ORDER_STATUS ){
                orderIds.add(newList[i].Id);
            }
        }

        if(orderIds.size() > 0)
            RollUpOrderItems(orderIds);

    }

    /**
     * @name RollUpOrderItems
     * @description Given a set of Activated Order ids, query the child Order Items and related Products to calculate Inventory levels
     * @param Set<Id> activatedOrderIds
     * @return void
    **/
    public static void RollUpOrderItems(Set<Id> activatedOrderIds) {
        List<OrderItem> orderItems = [SELECT Id, Quantity, OrderId, Product2Id FROM OrderItem WHERE OrderId IN:activatedOrderIds];
        Set<Id> product2Ids = initializeProduct2Ids(orderItems);
        Map<Id, Product2> productMap = new Map<Id, Product2>([SELECT Id, Quantity_Ordered__c FROM Product2 WHERE Id IN :product2Ids]);
        AggregateResult[] groupedResult = [SELECT Product2Id, SUM(Quantity) totalQuantity FROM OrderItem WHERE Product2Id IN :productMap.keySet() GROUP BY Product2Id];

        for(AggregateResult result : groupedResult)  {
            productMap.get((String)result.get('Product2Id')).Quantity_Ordered__c = Integer.valueOf(result.get('totalQuantity'));
        }
        update productMap.values();

    }

    /**
     *  Initialize products 2 ids
     */
    public static Set<Id> initializeProduct2Ids(List<OrderItem> orderItems) {
        Set<Id> product2Ids = new Set<Id>();

        for(OrderItem item :orderItems) {
            product2Ids.add(item.Product2Id);
        }

        return product2Ids;
    }
}